package wb.btn;

import javax.swing.Action;

/**
 * Botão transparente, sem borda.
 * @author cesar
 *
 */
public class AwesomeTranparentButton extends AwesomeButton {

	public AwesomeTranparentButton() {
		super();
		applyStyle();
	}

	@Override
	protected void applyStyle() {
		super.applyStyle();
		setOpaque(false);
		setContentAreaFilled(false);
		setBorderPainted(false);
	}
	
	public AwesomeTranparentButton(Action action) {
		super(action);
		applyStyle();
	}

	public AwesomeTranparentButton(String string) {
		super(string);
		applyStyle();
	}

}
