package wb.btn;

import java.awt.Color;
import java.awt.Dimension;
import java.util.Objects;

import javax.swing.ImageIcon;
import javax.swing.SwingConstants;

public class ScrollBarButton extends AwesomePaintedButton {

	public ScrollBarButton(int orientations) {

		setPreferredSize(new Dimension(20, 15));
		setBackground(Color.WHITE);
		setBorderPainted(false);

		String icon = null;
		String hoverIcon = null;

		if (orientations == SwingConstants.BOTTOM) {
			icon = "arrow_up.png";
			hoverIcon = "arrow_up_hover.png";
		} else if (orientations == SwingConstants.TOP) {
			icon = "arrow_down.png";
			hoverIcon = "arrow_down_hover.png";
		}
		ClassLoader classLoader = getClass().getClassLoader();
		setIcon(new ImageIcon(Objects.requireNonNull(classLoader.getResource(icon))));
		setRolloverIcon(new ImageIcon(Objects.requireNonNull(classLoader.getResource(hoverIcon))));

	}

	@Override
	protected void configureColorFocusListener() {}
	
}