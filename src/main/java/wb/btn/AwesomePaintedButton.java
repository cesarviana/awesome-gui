package wb.btn;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.UIManager;
import javax.swing.border.Border;

/**
 * Botão com borda e cor de fundo.
 * @author cesar
 *
 */
public class AwesomePaintedButton extends AwesomeButton {

	public AwesomePaintedButton() {
		super();
	}
	
	public AwesomePaintedButton(String string) {
		super(string);
	}

	@Override
	protected void applyStyle() {
		super.applyStyle();
		super.setContentAreaFilled(false);
		setBorderPainted(true);
		setBorder(UIManager.getBorder("button.default.border"));
		setSize(new Dimension(120, 45));
		setOpaque(true);
		setFocusPainted(false);
		
		setBackground(UIManager.getColor("general.background"));
		
		configureColorFocusListener();
		configureColorMouseListener();
	}

	private void configureColorMouseListener() {
		
		Border originalBorder = getBorder();
		
		addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				setBackground(UIManager.getColor("button.focused.background"));
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				setBackground(UIManager.getColor("button.pressed.background"));
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				setBorder(originalBorder);
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				setBorder(UIManager.getBorder("button.entered.border"));
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {}
		});
	}

	protected void configureColorFocusListener() {
		addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent e) {
				setBackground(UIManager.getColor("button.default.background"));
			}
			@Override
			public void focusGained(FocusEvent e) {
				setBackground(UIManager.getColor("button.focused.background"));
			}
		});
	}
	
	@Override
	public Insets getInsets() {
		return UIManager.getInsets("button.default.insets");
	}
	
	
	
}
