package wb.btn;

import icones.Icones;

import javax.swing.*;

public class TransparentCloseButton extends AwesomeTranparentButton {

	public TransparentCloseButton(Action action) {
		super(action);
        setIcon(Icones.get(Icones.CLOSE));
        setRolloverIcon(Icones.get(Icones.CLOSE_HOVER));
	}

	public String getText() {
		return "";
	}

}
