package wb.btn;

import icones.Icones;

import javax.swing.*;

public class TransparentIconifyButton extends AwesomeTranparentButton {

	public TransparentIconifyButton(Action action) {
		super(action);
        setIcon(Icones.get(Icones.ICONIFY));
        setRolloverIcon(Icones.get(Icones.ICONIFY_HOVER));
	}
	
	public String getText() {
		return "";
	}

}
