package wb.btn;
public interface WindowButtonPanelListener {
	public void closePressed();

	public void minimizePressed();

	public void maximizePressed();
}