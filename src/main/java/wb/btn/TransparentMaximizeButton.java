package wb.btn;

import javax.swing.*;

public class TransparentMaximizeButton extends AwesomeTranparentButton {

	public TransparentMaximizeButton(Action action) {
		super(action);
        setIcon(new ImageIcon(getClass().getResource("maximize.png")));
        setRolloverIcon(new ImageIcon(getClass().getResource("maximize_hover.png")));
	}
	
	public String getText() {
		return "";
	}
	
}
