package wb.btn;

import factory.GuiFactorySimpleFactory;

import java.awt.Cursor;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;

/**
 * Define borda do botão, cursor, e cor de fundo ao pressionar.
 * @author cesar
 *
 */
public class AwesomeButton extends JButton {

	public AwesomeButton(){
		GuiFactorySimpleFactory.createGuiFactory();
		applyStyle();
	}
	
	public AwesomeButton(Action action) {
		super(action);
		applyStyle();
	}

	public AwesomeButton(String string) {
		super(string);
		applyStyle();
	}

	protected void applyStyle() {
		setBorder(BorderFactory.createEmptyBorder());
		setCursor(new Cursor(Cursor.HAND_CURSOR));
	}
	
}
