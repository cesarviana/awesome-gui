package wb.btn;

import javax.swing.*;

public class DropdownIconButton extends AwesomeButton {

    public DropdownIconButton(Action... actions) {
        JPopupMenu popup = new JPopupMenu();
        for (Action action : actions) {
            popup.add(action);
        }
        setComponentPopupMenu(popup);
        addActionListener(e -> popup.show(DropdownIconButton.this, 0, DropdownIconButton.this.getHeight()));
    }
}
