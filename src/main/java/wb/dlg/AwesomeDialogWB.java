package wb.dlg;

import factory.AwesomeDialog;
import factory.GuiFactorySimpleFactory;
import icones.Icones;
import javatips.ComponentMover;
import javatips.ComponentResizer;
import wb.btn.AwesomePaintedButton;
import wb.pnl.AwesomeScrollPane;
import wb.pnl.TitleBarPanel;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import java.awt.*;

public class AwesomeDialogWB extends AwesomeDialog {

	private final JPanel contentPanel = new JPanel();
	private final TitleBarPanel titleBar;
	private final JTextPane textPane;

	public static void main(String[] args) {
		try {
			AwesomeDialogWB dialog = (AwesomeDialogWB) GuiFactorySimpleFactory.createGuiFactory().createDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
			dialog.setTitle("Titulo Diálogo");
			dialog.setText("Texto da mensagem");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public AwesomeDialogWB() {
		setBounds(100, 100, 630, 430);
		setUndecorated(true);
		setLocationRelativeTo(null);
		
		ComponentResizer cr = new ComponentResizer();
		cr.registerComponent(this);
		ComponentMover cm = new ComponentMover();
		cm.registerComponent(this);
		cm.setDragInsets(new Insets(20, 20, 20, 20));
		
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(0, 0, 0, 0));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			titleBar = new TitleBarPanel();
			titleBar.setParent(this);
			contentPanel.add(titleBar, BorderLayout.NORTH);
		}
		{	
			SimpleAttributeSet center = new SimpleAttributeSet();
			StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
			{
				JPanel panel = new JPanel();
				//JLay
				contentPanel.add(panel, BorderLayout.CENTER);
				textPane = new JTextPane();
				textPane.setOpaque(false);
				
				textPane.setEditable(false);
				textPane.setMargin(new Insets(20, 20, 20, 20));
				StyledDocument doc = textPane.getStyledDocument();
				doc.setParagraphAttributes(0, doc.getLength(), center, false);
				
				panel.setLayout(new BorderLayout(0, 0));
				
				textPane.setFont(UIManager.getFont("dialog.font"));
				textPane.setForeground(UIManager.getColor("general.font.color"));
				textPane.setAlignmentY(CENTER_ALIGNMENT);
				
				JScrollPane scroll = new AwesomeScrollPane();
				panel.add(scroll);
				scroll.setViewportView(textPane);
				scroll.setOpaque(false);
				scroll.getViewport().setOpaque(false);
				scroll.setBorder(BorderFactory.createEmptyBorder());
				
				textPane.setPreferredSize(new Dimension(400,400));
				{
                    JLabel lblNewLabel = new JLabel(Icones.get(Icones.ICON));
					lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
					panel.add(lblNewLabel, BorderLayout.NORTH);
				}
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBorder(new EmptyBorder(10, 10, 10, 10));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			{
				JButton okButton = new AwesomePaintedButton();
				okButton.setText("OK");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new AwesomePaintedButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
	
	@Override
	public void setTitle(String title) {
		titleBar.setTitle(title);
		super.setTitle(title);
	}
	
	@Override
	public void setText(String text) {
		textPane.setText(text);
	}

	@Override
	public void setIcon(ImageIcon icon) {
		
	}
}
