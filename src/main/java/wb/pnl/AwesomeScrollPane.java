package wb.pnl;

import wb.btn.ScrollBarButton;

import javax.swing.*;
import javax.swing.plaf.basic.BasicScrollBarUI;

public class AwesomeScrollPane extends JScrollPane {

	private static BasicScrollBarUI ui2;

	public AwesomeScrollPane() {
		ui2 = new BasicScrollBarUI() {
			@Override
			protected void configureScrollBarColors() {
				this.trackColor = UIManager.getColor("scroll.bar.track.color");
				this.thumbColor = UIManager.getColor("scroll.bar.thumb.color");
			}

			@Override
			protected JButton createDecreaseButton(int orientation) {
				return new ScrollBarButton(SwingConstants.BOTTOM);
			}

			@Override
			protected JButton createIncreaseButton(int orientation) {
				return new ScrollBarButton(SwingConstants.TOP);
			}
		};
        // Por algum motivo, aplica somente na segunda chamada ao método
		getVerticalScrollBar().setUI(ui2);
		getVerticalScrollBar().setUI(ui2);
		
		setHorizontalScrollBarPolicy(HORIZONTAL_SCROLLBAR_NEVER);
	}

}
