package wb.pnl;

import icones.Icones;
import wb.btn.TransparentCloseButton;
import wb.btn.TransparentIconifyButton;
import wb.btn.TransparentMaximizeButton;
import wb.btn.WindowButtonPanelListener;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collection;

public class WindowButtonsPanel extends JPanel {
		private Collection<WindowButtonPanelListener> listeners = new ArrayList<>();
	private final Action action_1 = new MaximizeAction();
		private final Action action_2 = new CloseAction();

		private final JButton btnMaximize;
		private final JButton btnMinimize;
		
		public WindowButtonsPanel() {

			Action action = new MinimizeAction();
			btnMinimize = new TransparentIconifyButton(action);
			add(btnMinimize);

			btnMaximize = new TransparentMaximizeButton(action_1);
			add(btnMaximize);

			JButton btnClose = new TransparentCloseButton(action_2);
			add(btnClose);
		}

		public void addListener(WindowButtonPanelListener listener) {
			listeners.add(listener);
		}

		private class MinimizeAction extends AbstractAction {
			public MinimizeAction() {
				putValue(NAME, "Minimizar");
				putValue(SHORT_DESCRIPTION, "Minimizar");
			}

			public void actionPerformed(ActionEvent e) {
				for (WindowButtonPanelListener windowButtonPanelListener : listeners) {
					windowButtonPanelListener.minimizePressed();
				}
			}
		}

		private class MaximizeAction extends AbstractAction {
			public MaximizeAction() {
				putValue(NAME, "Maximizar");
				putValue(SHORT_DESCRIPTION, "Maximizar");
                putValue(SMALL_ICON, Icones.get(Icones.MAXIMIZE));
			}

			public void actionPerformed(ActionEvent e) {
				for (WindowButtonPanelListener windowButtonPanelListener : listeners) {
					windowButtonPanelListener.maximizePressed();
				}
			}

		}

		private class CloseAction extends AbstractAction {
			public CloseAction() {
				putValue(NAME, "Fechar");
				putValue(SHORT_DESCRIPTION, "Fechar");
				putValue(
						SMALL_ICON,
                        Icones.get(Icones.CLOSE));
			}

			public void actionPerformed(ActionEvent e) {
				for (WindowButtonPanelListener windowButtonPanelListener : listeners) {
					windowButtonPanelListener.closePressed();
				}
			}
		}

		public void setDialogMode() {
			btnMaximize.setVisible(false);
			btnMinimize.setVisible(false);
		}
	}