package wb.pnl;

import factory.GuiFactorySimpleFactory;
import wb.btn.WindowButtonPanelListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;

public class TitleBarPanel extends JPanel {

	private JLabel lblTitle;
	private WindowButtonsPanel buttonsPanel;

	/**
	 * Create the panel.
	 */
	public TitleBarPanel() {
		
		setLayout(new BorderLayout(0, 0));
		lblTitle = new JLabel(UIManager.getString("titlebar.text"));
		lblTitle.setHorizontalAlignment(JLabel.CENTER);
		lblTitle.setFont(UIManager.getFont("titlebar.font"));
		add(lblTitle, BorderLayout.CENTER);

		buttonsPanel = new WindowButtonsPanel();
		add(buttonsPanel, BorderLayout.EAST);
		// lblTitle.setFont(factory.GuiFactorySimpleFactory.createGuiFactory().getTitleBarFont());

	}

	public void setTitle(String text) {
		lblTitle.setText(text);
	}

	public void setParent(JFrame frame) {

		if (frame != null) {
			frame.setIconImage(GuiFactorySimpleFactory.createGuiFactory()
					.getDefaultIcon());
		}
		
		buttonsPanel.addListener(new WindowButtonPanelListener() {

			@Override
			public void minimizePressed() {
				frame.setState(Frame.ICONIFIED);
			}

			@Override
			public void maximizePressed() {
				if(frame.getExtendedState()==JFrame.MAXIMIZED_BOTH) {
					frame.setExtendedState(JFrame.NORMAL);
				} else {
					frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
				}
			}

			@Override
			public void closePressed() {
				frame.dispatchEvent(new WindowEvent(frame,
						WindowEvent.WINDOW_CLOSING));
			}
		});
	}
	
	public void setParent(JDialog dlg){
		
		buttonsPanel.addListener(new WindowButtonPanelListener() {

			@Override
			public void minimizePressed() {}

			@Override
			public void maximizePressed() {}

			@Override
			public void closePressed() {
				dlg.dispatchEvent(new WindowEvent(dlg,
						WindowEvent.WINDOW_CLOSING));
			}
		});
		buttonsPanel.setDialogMode();
	}
}
