package wb.frm;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Insets;

import factory.GuiFactorySimpleFactory;
import javatips.ComponentMover;
import javatips.ComponentResizer;
import wb.pnl.TitleBarPanel;

import javax.swing.*;

public class AwesomeFrameWB extends JFrame {

	private TitleBarPanel titleBarPanel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(() -> {
			try {
				JFrame frame = GuiFactorySimpleFactory.createGuiFactory().createJFrame();
				frame.setTitle("Título");
				frame.setVisible(true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AwesomeFrameWB() {
		
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setUndecorated(true);
		
		setSize(800,600);
		setLocationRelativeTo(null);
		getRootPane().setBorder(UIManager.getBorder("frame.border"));
		
		ComponentResizer cr = new ComponentResizer();
		cr.registerComponent(this);
		ComponentMover cm = new ComponentMover();
		cm.registerComponent(this);
		cm.setDragInsets(new Insets(20, 20, 20, 20));

		JPanel contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		titleBarPanel = new TitleBarPanel();
		titleBarPanel.setParent(this);
		contentPane.add(titleBarPanel, BorderLayout.NORTH);
	}

	@Override
	public void setTitle(String title) {
		super.setTitle(title);
		titleBarPanel.setTitle(title);
	}

}
