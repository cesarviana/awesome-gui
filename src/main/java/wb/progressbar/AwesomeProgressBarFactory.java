package wb.progressbar;
import javax.swing.BorderFactory;
import javax.swing.JProgressBar;

import java.awt.Dimension;

public final class AwesomeProgressBarFactory {
	/**
	 * @wbp.factory
	 */
	public static JProgressBar createJProgressBar() {
		JProgressBar progressBar = new JProgressBar();
		progressBar.setPreferredSize( new Dimension(10, 10));
		progressBar.setBorder(BorderFactory.createEmptyBorder());
		return progressBar;
	}
}