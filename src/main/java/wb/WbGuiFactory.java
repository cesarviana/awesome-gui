package wb;

import cmb.AwesomeComboBox;
import factory.AwesomeDialog;
import factory.GuiFactory;
import wb.dlg.AwesomeDialogWB;
import wb.frm.AwesomeFrameWB;
import wb.pnl.AwesomeScrollPane;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class WbGuiFactory implements GuiFactory {

	private static final Insets BUTTONS_INSETS  = new Insets(5, 30, 5, 30);
	private static final Font FONT = new Font("Times New Roman", Font.ITALIC, 20);
	private static final Color FONT_COLOR = new Color(128,128,128);
	private static final Color SCROLL_THUMB_COLOR = new Color(230,230,230);
	private static final Color BACKGROUND_COLOR = new Color(249,249,249);
	private static final Color BACKGROUND_COLOR_FOCUSED_BUTTON = Color.WHITE;
	private static final Color BACKGROUND_COLOR_PRESSED_BUTTON = new Color(203,249,205);
	private static final Border FRAME_BORDER = BorderFactory.createLineBorder(Color.DARK_GRAY,4);
	private static final Border BUTTONS_BORDER = BorderFactory.createLineBorder(new Color(195,195,195),1);
	
	public WbGuiFactory() {
		applyUIManager();
	}
	
	@Override
	public void applyUIManager() {
		UIManager.put("general.background", BACKGROUND_COLOR);
		UIManager.put("general.font", FONT);
		UIManager.put("general.font.color", FONT_COLOR);
		UIManager.put("titlebar.font", UIManager.getFont("general.font").deriveFont(Font.ITALIC, 34));
		UIManager.put("titlebar.text", "Título");
		UIManager.put("button.pressed.background", BACKGROUND_COLOR_PRESSED_BUTTON);
		UIManager.put("button.focused.background", BACKGROUND_COLOR_FOCUSED_BUTTON);
		UIManager.put("button.entered.border", BorderFactory.createLineBorder(Color.DARK_GRAY,1));
		UIManager.put("button.default.background", BACKGROUND_COLOR);
		UIManager.put("button.default.insets", BUTTONS_INSETS);
		UIManager.put("button.default.border",BUTTONS_BORDER);
		UIManager.put("button.default.font",UIManager.get("titlebar.font"));
		UIManager.put("dialog.font", UIManager.getFont("general.font").deriveFont(Font.PLAIN));
		UIManager.put("frame.border", FRAME_BORDER);
		UIManager.put("dialog.border", FRAME_BORDER);
		UIManager.put("scroll.bar.thumb.color", SCROLL_THUMB_COLOR);
		UIManager.put("scroll.bar.track.color", BACKGROUND_COLOR);
		UIManager.put("Panel.background", UIManager.getColor("general.background"));
	}
	
	@Override
	public JFrame createJFrame() {
		return new AwesomeFrameWB();
	}
	
	@Override
	public AwesomeDialog createDialog() {
		return new AwesomeDialogWB();
	}
	
	@Override
	public JDialog createOkCancelDialog(String string) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JScrollPane createScrollPane() {
		return new AwesomeScrollPane();
	}
	
	@Override
	public BufferedImage getDefaultIcon() {
		try {
            return ImageIO.read(getClass().getResource("icon.png"));
		} catch (IOException e) {
			e.printStackTrace();
			return new BufferedImage(0, 0, BufferedImage.TYPE_BYTE_GRAY);
		}
	}
	
	@Override
	public <T> JComboBox<T> createJComboBox(Class<T> type){
		return new AwesomeComboBox<T>();
	}
	
}
