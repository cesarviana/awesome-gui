package factory;

import javax.swing.ImageIcon;
import javax.swing.JDialog;

public abstract class AwesomeDialog extends JDialog {

	public abstract void setText(String text);
	public abstract void setIcon(ImageIcon icon);
	
}
