package factory;

import java.awt.image.BufferedImage;

import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

public interface GuiFactory {

	public JFrame createJFrame();
	public AwesomeDialog createDialog();
	public JDialog createOkCancelDialog(String string);
	public void applyUIManager();
	public BufferedImage getDefaultIcon();
	public JScrollPane createScrollPane();
	public <T> JComboBox<T> createJComboBox(Class<T> type);
	
	
}
