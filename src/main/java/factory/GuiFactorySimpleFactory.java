package factory;

import wb.WbGuiFactory;

public class GuiFactorySimpleFactory {
	
	private static GuiFactory guiFactory;
	
	public static GuiFactory createGuiFactory(){
		if(guiFactory==null)
			guiFactory = new WbGuiFactory();
		return guiFactory;
	}

}
