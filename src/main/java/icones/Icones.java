package icones;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.net.URL;

public class Icones {
    public static final String MAXIMIZE = "maximize.png";
    public static final String CLOSE = "close.png";
    public static final String ICONIFY = "iconify.png";
    public static final String ICONIFY_HOVER = "iconify_hover.png";
    public static final String ICON = "icon.png";
    public static final String MAXIMIZE_HOVER = "maximize_hover.png";
    public static final String CLOSE_HOVER = "close_hover.png";

    public static Icon get(String name) {
        return new ImageIcon(getResource(name));
    }

    public static Image getImage(String name) {
        try {
            return ImageIO.read(getResource(name));
        } catch (IOException e) {
            throw new RuntimeException("Imagem não encontrada", e);
        }
    }

    private static URL getResource(String name) {
        return Icones.class.getClassLoader().getResource(name);
    }
}
