package tbl;

import javax.swing.*;
import java.awt.*;

public class AwesomeTable extends JTable {
    public AwesomeTable() {
        setRowHeight(50);
        setGridColor(Color.WHITE);
    }
}
