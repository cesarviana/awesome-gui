package awesome_gui;


public class DialogTests {

	public static void main(String[] args) {
		
		GuiFactory gf = GuiFactorySimpleFactory.createGuiFactory();
		
		AwesomeDialog dialog = gf.createDialog();
		dialog.setText("Você tem plena certeza do que está prestes a fazer?");
		dialog.setVisible(true);
		
	}
	
}
